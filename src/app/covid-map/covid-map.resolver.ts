import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CovidCase } from './covid-map-type';
import { CovidService } from './covid.service';

@Injectable({
  providedIn: 'root'
})
export class CovidMapResolver implements Resolve<any>
{
  /**
   * Constructor
   */
   constructor(
     private _covidService: CovidService
     )
   {
   }


  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Resolver
   *
   * @param route
   * @param state
   */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CovidCase[]>
    {
      return this._covidService.getCovidCase();
    }

}

@Injectable({
  providedIn: 'root'
})
export class CovidStateResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _covidService: CovidService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CovidCase>
    {

        return this._covidService.getCovidCaseForSingleState(route.paramMap.get('id'))
          .pipe(
              // Error here means the requested state is not available
              catchError((error) => {

                  // Log the error
                  console.error(error);

                  // Get the parent url
                  const parentUrl = state.url.split('/').slice(0, -1).join('/');

                  // Navigate to there
                  this._router.navigateByUrl(parentUrl);

                  // Throw an error
                  return throwError(error);
              })
          );
    }
}



