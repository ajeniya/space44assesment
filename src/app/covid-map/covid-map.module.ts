import { NgModule } from '@angular/core';

import { covidRoutes } from './covid-map-routing';
import { CovidMapComponent } from './covid-map.component';
import { RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { FullViewComponent } from './full-view/full-view.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';

import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'app/shared/shared.module';


@NgModule({
  declarations: [
    CovidMapComponent,
    DetailsComponent,
    FullViewComponent,
  ],
  imports: [
    RouterModule.forChild(covidRoutes),
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB7t9vt41C2XMtMMSjn_5T9P3Z7CwgJOL4'
    }),
    SharedModule
  ],
  providers: [
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'fill'}}
  ]
})
export class CovidMapModule { }
