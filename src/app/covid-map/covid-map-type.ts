export interface CovidCase
{
  date                        : number;
  state                       : string;
  positive                    : number;
  probableCases?              : number;
  negative?                   : number;
  pending?                    : number;
  totalTestResultsSource      : string;
  totalTestResults            : number;
  hospitalizedCurrently?      : number;
  hospitalizedCumulative?     : number;
  inIcuCurrently?             : number;
  inIcuCumulative?            : number;
  onVentilatorCurrently?      : number;
  onVentilatorCumulative?     : number;
  recovered?                  : number;
  lastUpdateEt                : string;
  dateModified?               : Date;
  checkTimeEt                 : string;
  death                       : number;
  hospitalized?               : number;
  hospitalizedDischarged?     : number;
  dateChecked?                : Date;
  totalTestsViral?            : number;
  positiveTestsViral?         : number;
  negativeTestsViral?         : number;
  positiveCasesViral?         : number;
  deathConfirmed?             : number;
  deathProbable?              : number;
  totalTestEncountersViral?   : number;
  totalTestsPeopleViral?      : number;
  totalTestsAntibody?         : number;
  positiveTestsAntibody?      : number;
  negativeTestsAntibody?      : number;
  totalTestsPeopleAntibody?   : number;
  positiveTestsPeopleAntibody?: number;
  negativeTestsPeopleAntibody?: number;
  totalTestsPeopleAntigen?    : number;
  positiveTestsPeopleAntigen? : number;
  totalTestsAntigen?          : number;
  positiveTestsAntigen?       : number;
  fips                        : string;
  positiveIncrease            : number;
  negativeIncrease            : number;
  total                       : number;
  totalTestResultsIncrease    : number;
  posNeg                      : number;
  dataQualityGrade?           : any;
  deathIncrease               : number;
  hospitalizedIncrease        : number;
  hash                        : string;
  commercialScore             : number;
  negativeRegularScore        : number;
  negativeScore               : number;
  positiveScore               : number;
  score                       : number;
  grade                       : string;
  lat?                        : number;
  log?                        : number;
  stateName?                  : string;
  stateLatAndLong?            : StateLanAndLong;
}

export interface StateLanAndLong
{
  state    : string;
  latitude : number;
  longitude: number;
  code     : string;
}

export interface Marker
{
	lat      : number;
	lng      : number;
  state?   : string;
  code?    : string;
	label?   : string;
  hash?    : string;
  radius?  : number;
	draggable: boolean;
}
