import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { CovidService } from '../covid.service';
import { google } from "google-maps";
declare var google: google;

import { MouseEvent } from '@agm/core';
import { CovidCase, Marker, StateLanAndLong } from '../covid-map-type';


@Component({
  selector    : 'app-full-view',
  templateUrl : './full-view.component.html',
  styleUrls   : ['./full-view.component.scss']
})
export class FullViewComponent implements OnInit, OnDestroy
{
  @ViewChild('matDrawer', { static: true }) matDrawer!: MatDrawer;
  public drawerWidth: number;
  public getScreenWidth: any;
  public getScreenHeight: any;

  // google maps zoom level
  zoom: number = 5;

  // initial center position for the map
  lat: number = 0;
  lng: number = 0;

  covidCases!: CovidCase[];
  selectedState!: CovidCase;
  states!: StateLanAndLong[];
  covidcaseWithLatLog = [];
  markers: Marker[] = []

  drawerMode!: 'side' | 'over';
  private _unsubscribeAll: Subject<any> = new Subject<any>();
  widthSize: any;
  headerTitle: string;

  /**
  * Constructor
  */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _covidService: CovidService,
    @Inject(DOCUMENT) private _document: any
  )
  {
    this.drawerWidth = 0;
    this.headerTitle = 'Covid Data';
  }


  // -----------------------------------------------------------------------------------------------------
  // Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {

    // initial center position for the map
    this.lat = 39.106667;
    this.lng = -94.676392;

    this.getStateLatAndLong();

    this.getAllCovidRecord();

    this.stateInfo();

    this.setTitle();

    this.matDrawerOnChange();

  }


  /**
   * On destroy
  */
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get all state with lat and long details
   */
  getStateLatAndLong()
  {
    // Get all state with Lat and Log
    this._covidService.stateInLatAndLong()
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((state: StateLanAndLong[]) => {

        this.states = state;

        // Mark for check
        this._changeDetectorRef.markForCheck();

    });

  }

  /**
   * Get the covid records
   */
  getAllCovidRecord()
  {
    // Get all the covid case
    this._covidService.covid$
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((cases: CovidCase[]) => {

        this.covidCases = cases;

        this.addStateLatLongToCovidCase();

        // Mark for check
        this._changeDetectorRef.markForCheck();

    });

  }

  /**
   * Get the selected state record
   */
  stateInfo()
  {
      // Get the state
      this._covidService.stateInfo$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((state: CovidCase) => {

          // Update the selected state
          this.selectedState = state;

          // Mark for check
          this._changeDetectorRef.markForCheck();

      });
  }

  /**
   * Update the title
   */
  setTitle()
  {
    // Set title
    this._covidService.title$
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((data: any) => {

        // Update the selected state
        this.headerTitle = data.stateName;

        // Mark for check
        this._changeDetectorRef.markForCheck();

    });

  }

  /**
   * On MatDrawer opened change
   */
  matDrawerOnChange()
  {
    // Subscribe to MatDrawer opened change
    this.matDrawer.openedChange.subscribe((opened) => {

      // Set the width of drawer content
      this.drawerWidth = 300;

      // Mark for check
      this._changeDetectorRef.markForCheck();

      if ( !opened )
      {
          // Remove the selected state when drawer closed
          this.drawerWidth = 0;

          this.headerTitle = 'Covid Data';

          // Mark for check
          this._changeDetectorRef.markForCheck();
      }

    });

  }

  /**
   * On backdrop clicked
   */
  onBackdropClicked(): void
  {
      // Go back
      this._router.navigate(['./'], {relativeTo: this._activatedRoute});

      // Mark for check
      this._changeDetectorRef.markForCheck();
  }


  clickedMarker(label: any, index: number) {
    this.headerTitle = label.state;
  }


  mapClicked($event: MouseEvent)
  {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }



  // -----------------------------------------------------------------------------------------------------
  // Private methods
  // -----------------------------------------------------------------------------------------------------

  // Iterate over the list of covid states and add
  // there state latitude and longitude
  private addStateLatLongToCovidCase()
  {

    this.covidCases.forEach((covidCaseByState: CovidCase) => {


      for (let index = 0; index < this.states.length; index++)
      {
          const stateList = this.states[index];

          if( stateList.code === covidCaseByState.state )
          {
              covidCaseByState['lat'] = stateList.latitude;
              covidCaseByState['log'] = stateList.longitude;
              covidCaseByState['stateName'] = stateList.state;


              this.markers.push({
                lat: stateList.latitude,
                lng: stateList.longitude,
                state: stateList.state.toUpperCase(),
                code: stateList.code.toLowerCase(),
                hash: covidCaseByState.hash,
                radius: covidCaseByState.hospitalized,
                draggable: true
              });

          }
      }

    })

  }


}
