import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FullViewComponent } from '../full-view/full-view.component';
import { CovidService } from '../covid.service';
import { Subject, takeUntil } from 'rxjs';
import { CovidCase } from '../covid-map-type';


@Component({
  selector       : 'app-details',
  templateUrl    : './details.component.html',
  encapsulation  : ViewEncapsulation.None
})
export class DetailsComponent implements OnInit, OnDestroy
{
  @ViewChild('searchNgForm') searchNgForm!: NgForm;

  private _unsubscribeAll: Subject<any> = new Subject<any>();
  stateToSearch: string;
  state!: CovidCase | any;
  searchForm!: FormGroup;
  covidCases!: CovidCase[];

    /**
     * Constructor
     */
    constructor(
      private _changeDetectorRef: ChangeDetectorRef,
      private _fullViewComponent: FullViewComponent,
      private _formBuilder: FormBuilder,
      private _covidService: CovidService,
      private _snackBar: MatSnackBar
  )
  {
    this.stateToSearch = '';
  }

  // -----------------------------------------------------------------------------------------------------
  // Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------


  ngOnInit(): void
  {
    // Open the drawer
    this._fullViewComponent.matDrawer.open();


    // Create the search form
    this.searchForm = this._formBuilder.group({
      search : ['', [Validators.required]],
    });

    // Get the state
    this._covidService.stateInfo$
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((state: CovidCase) => {
        this.state = state;

        // Mark for check
        this._changeDetectorRef.markForCheck();
    });


    // Since there is no endpoint to search
    // I will fetch all the covid data then filtered when search
    this._covidService.covid$
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((cases: CovidCase[]) => {
        this.covidCases = cases;

    });
  }


  /**
   * On destroy
   */
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.complete();
  }


  // -----------------------------------------------------------------------------------------------------
  // Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Filter by search query
   *
   * @param query
   */
  filterBySearch(search: string): void
  {
    this.stateToSearch = search;

  }


  searchCovid(): void
  {
    // Return if the form is invalid
    if ( this.searchForm.invalid )
    {
        return;
    }

    // Disable the form
    this.searchForm.disable();

    if ( this.stateToSearch !== '' )
    {
      let searched = this.covidCases.filter((covidByState) => covidByState.stateName?.toLowerCase().includes(this.stateToSearch.toLowerCase()));

      if(searched.length === 0)
      {
        this._snackBar.open('No search found', 'Close');

        // Re-enable the form
        this.searchForm.enable();

        // Reset the form
        this.searchNgForm.resetForm();

        return;
      }

      if(searched)
      {

        this.state = searched[0];

        // Set title to state name
        this._covidService._title.next(this.state);

        // Mark for check
        this._changeDetectorRef.markForCheck();


        // Re-enable the form
        this.searchForm.enable();

        // Reset the form
        this.searchNgForm.resetForm();
      }

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }
  }

}
