import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CovidService } from './covid.service';

describe('CovidService', () => {

  // We declare the variables that we'll use for the Test Controller and for our Service
  let httpTestingController: HttpTestingController;

  let service: CovidService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports  : [HttpClientTestingModule],
      providers: [CovidService],
    });


    // We inject our service (which imports the HttpClient) and the Test Controller
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(CovidService);

  });


  afterEach(() => {
    httpTestingController.verify();
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the current title', () => {

    var setTitle: string | undefined;

    service.title$.subscribe((title) => {
      setTitle = title;
    });

    expect(setTitle).toBe('Covid Data');

  });


  it('should return Observable of covid lists', () => {

    service.getCovidCase();

    service.covid$.subscribe((covidlists) => {

      expect(covidlists).toBeNull()

    });

  });


  it('should return Observable of covid that match the right state', () => {

    service.getCovidCaseForSingleState('ks');

    service.stateInfo$.subscribe((titleReaponse) => {

       expect(titleReaponse.stateName).toEqual('Kansas');

    });



  });


});
