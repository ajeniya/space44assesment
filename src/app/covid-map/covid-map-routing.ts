import { Route } from "@angular/router";
import { CovidMapComponent } from "./covid-map.component";
import { CovidMapResolver, CovidStateResolver } from "./covid-map.resolver";
import { DetailsComponent } from "./details/details.component";
import { FullViewComponent } from "./full-view/full-view.component";

export const covidRoutes: Route[] = [
  {
      path     : '',
      component: CovidMapComponent,
      resolve  : {
          tags: CovidMapResolver
      },
      children : [
        {
            path     : '',
            component: FullViewComponent,
            children : [
                {
                    path         : ':id',
                    component    : DetailsComponent,
                    resolve  : {
                      tags: CovidStateResolver
                  },
                }
            ]
        }
      ]
  }
];
