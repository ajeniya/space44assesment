import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CovidCase } from './covid-map-type';
import { States } from './us-state';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CovidService
{

  // Environment URL
  environmentUrl = environment.apiUrl;

  // Private
  private _covidCases: BehaviorSubject<CovidCase[] | any> = new BehaviorSubject(null);
  private _stateInfo: BehaviorSubject<CovidCase[] | any> = new BehaviorSubject(null);

  // Public
  public _title: BehaviorSubject<string> = new BehaviorSubject('Covid Data');

  constructor(private _httpClient: HttpClient)
  {
    this.environmentUrl = environment.apiUrl;
  }

  /**
   * Getter for covid cases
   */
  get covid$(): Observable<CovidCase[]>
  {
      return this._covidCases.asObservable();
  }

  /**
   * Getter for covid state
   */
  get stateInfo$(): Observable<CovidCase>
  {
      return this._stateInfo.asObservable();
  }


  /**
   * Getter for title
   */
  get title$(): Observable<string>
  {
      return this._title.asObservable();
  }


  /**
   * Getting for all state as observable
   */
  stateInLatAndLong()
  {
      return of(States);
  }


   // -----------------------------------------------------------------------------------------------------
   // @ Public methods
   // -----------------------------------------------------------------------------------------------------

   /**
    * Expose the covid data for easy access
    */
   getCurrentCovidCase(response: CovidCase[])
   {
      return  this._covidCases.next(response);
   }

    /**
     * Get covid case list
     */
    getCovidCase(): Observable<CovidCase[]>
    {
        return this._httpClient.get<CovidCase[]>(`${this.environmentUrl}/v1/states/current.json`).pipe(
            tap((response) => {
                this.getCurrentCovidCase(response)
            })
        );
    }


  /**
   * Expose the covid state data for easy access
   */
   getCurrentSingleState(response: CovidCase)
   {
      return  this._stateInfo.next(response);
   }


    /**
     * Get covid case by state
     */
    getCovidCaseForSingleState(state: string | null): Observable<CovidCase>
    {
        return this._httpClient.get<CovidCase>(`${this.environmentUrl}/v1/states/${state}/current.json`).pipe(
            tap((response) => {
              this.getCurrentSingleState(response)
            })
        );
    }


}
