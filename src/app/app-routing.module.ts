import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {path: '', pathMatch : 'full', redirectTo: 'covid'},
  {path: 'covid', loadChildren: () => import('../app/covid-map/covid-map.module').then(m => m.CovidMapModule)}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
