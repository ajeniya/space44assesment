# Space44

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.2.

## Running unit tests

Run `ng test` to execute the unit tests.

## Covid Data (App name)

## Api source
https://api.covidtracking.com

This is an application the show the current status of covid information on map (US based only)
Upon clicking the maker of any of the state it show a tip you can clicked the display an additional information about the state and can also search for sate using name.

The solution focus on frontend angular 13 best practice.

## Architectural 

Tailwind css
Tailwind is both an excellent and easy to use CSS framework for quick UI development. It works seamlessly on small projects or enterprise grade projects. Regardless of your CSS experience level.

## Angular Material

Why I choose angular material
* It facilitates the development process and most important helps build quickly applications with the provided high quality and maintainable components.
* Unified web application across different devices and different screen sizes (responsive apps)
* Provide tools to developers the build their own custom components with common interaction patterns.
* Since it’s built by the Angular team, it can be integrated into Angular applications seamlessly.

## Note
I was unable to write the unit test for the component due to time constraint. I hope you bear with me, an hope does not limit my chance of getting the offer.

Some Issue about the api
The API does not have the any endpoint the search, So you may find in my code that am doing some filtering a large data set with is a bad practise. This is the most I can achieved due to the endpoint.


## Link to my resume
http://deji.retailboosters.ng/images/oladimejiajeniya-latest-php.pdf


## My Code
https://bitbucket.org/ajeniya/gloos-v2/
https://bitbucket.org/ajeniya/angular-unit-testing



## Public profile
http://deji.retailboosters.ng/


To the reviewer happy coding.

